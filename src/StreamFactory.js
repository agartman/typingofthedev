import Kefir from "kefir"

export function wordStreamFactory(target, eventName, targetWord) {
  return Kefir.fromEvents(target, eventName)
    .filter(x => x.target.value === targetWord)
    .take(1) //Just take the first match, so this stream can end
}

export function wordStreamTimerFactory(
  target,
  eventName,
  targetWord,
  timeToEnd
) {
  var timerStream = Kefir.later(timeToEnd, 1)
  var wordStream = wordStreamFactory(target, eventName, targetWord)
  return wordStream.takeUntilBy(timerStream)
}
