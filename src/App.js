import React, { Component } from "react"
import styled from "styled-components"
import KefirSandbox from "./KefirSandbox"
const AppContainer = styled.div`
   {
    text-align: center;
  }
`

const AppHeader = styled.header`
   {
    background-color: #222;
    height: 50px;
    padding: 20px;
    color: white;
  }
`

const AppTitle = styled.h1`
   {
    font-size: 1.5em;
  }
`

class App extends Component {
  render() {
    return (
      <AppContainer>
        <AppHeader>
          <AppTitle>Type race</AppTitle>
        </AppHeader>
        <KefirSandbox />
      </AppContainer>
    )
  }
}

export default App
