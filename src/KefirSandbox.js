import React, { Component } from "react"
import Kefir from "kefir"
import _ from "lodash"
import { wordStreamFactory } from "./StreamFactory"

let incomingWords = ["Unity", "Stream", "Car", "Bus", "Bike", "Orange"]
let raceInputFieldSelector = `input[name="raceInput"]`

let clearAndFocus = selector => {
  let field = document.querySelector(selector)
  field.value = ""
  field.focus()
}

var wordStrBuilder = word =>
  wordStreamFactory(
    document.querySelector(raceInputFieldSelector),
    "input",
    word
  )

var combinedWordExpiresStream = (callback, word) =>
  wordStrBuilder(word)
    .takeUntilBy(Kefir.later(5000, "expired"))
    .onEnd(() => {
      callback()
    })

export default class KefirSandbox extends Component {
  constructor(context) {
    super(context)

    this.state = {
      incomingWords: incomingWords,
      successfulWords: [],
      lives: 3,
      startingInterval: 3000
    }
  }

  componentDidMount() {
    var pool = Kefir.pool()

    var launchNextWord = () => {
      clearAndFocus(raceInputFieldSelector)
      this.setState({ incomingWords: _.drop(this.state.incomingWords, 1) })

      var newcombined = combinedWordExpiresStream(
        launchNextWord,
        this.state.incomingWords[0]
      )

      pool.plug(newcombined)
    }
    launchNextWord()

    pool.onValue(x => {
      console.log(x)
      this.setState({
        successfulWords: _.concat(this.state.successfulWords, [x.target.value])
      })
    })
  }
  render() {
    return (
      <div>
        <h1>{this.state.incomingWords[0]}</h1>
        <h1>Lives {this.state.lives}</h1>
        <input name="raceInput" type="text" title="raceInput" />

        <h3>Success</h3>
        <ul>
          {this.state.successfulWords.map((word, index) => (
            <li key={index}>{word}</li>
          ))}
        </ul>
      </div>
    )
  }
}
